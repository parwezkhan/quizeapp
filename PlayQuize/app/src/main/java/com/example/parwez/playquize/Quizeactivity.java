package com.example.parwez.playquize;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Quizeactivity extends AppCompatActivity {

    private     Questionlibrary qlib= new Questionlibrary();
    private TextView mscoreview ;
    private TextView mquestionview;
    private Button mchoice1;
    private Button mchoice2;
    private Button mchoice3;
    private Button mchoice4;
    private  String manswer;
    private  int mscore=0;
    private  int mquestionNumber=0;
    // private Button q= (Button) findViewById(R.id.quit);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizeactivity);

        mscoreview= (TextView) findViewById(R.id.score);
        mquestionview=(TextView) findViewById(R.id.question);
        mchoice1=(Button) findViewById(R.id.choice1);
        mchoice2=(Button) findViewById(R.id.choice2);
        mchoice3=(Button) findViewById(R.id.choice3);
        mchoice4=(Button) findViewById(R.id.choice4);

         updatequestion();

        /*   q.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  finish();
                  System.exit(0);
              }
          }); */


        mchoice1.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                if(mchoice1.getText() == manswer)
                {
                    mscore = mscore +1;
                    updateScore(mscore);
                    Toast.makeText(Quizeactivity.this,"Correct",Toast.LENGTH_SHORT).show();
                    updatequestion();
                }
                else
                {
                    Toast.makeText(Quizeactivity.this,"Wrong",Toast.LENGTH_SHORT).show();
                }
            }
        } );

        mchoice2.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                if(mchoice2.getText()== manswer)
                {
                    mscore = mscore +1;
                    Toast.makeText(Quizeactivity.this,"Correct",Toast.LENGTH_SHORT).show();
                    updatequestion();
                }
                else
                {
                    Toast.makeText(Quizeactivity.this,"Wrong",Toast.LENGTH_SHORT).show();

                }
            }
        } );

        mchoice3.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                if(mchoice3.getText()== manswer)
                {
                    mscore = mscore +1;
                    updateScore(mscore);
                    Toast.makeText(Quizeactivity.this,"Correct",Toast.LENGTH_SHORT).show();
                    updatequestion();
                }
                else
                {
                    Toast.makeText(Quizeactivity.this,"Wrong",Toast.LENGTH_SHORT).show();

                }
            }
        } );

        mchoice4.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                if(mchoice4.getText()== manswer)
                {
                    mscore = mscore +1;
                    updateScore(mscore);
                    Toast.makeText(Quizeactivity.this,"Correct",Toast.LENGTH_SHORT).show();
                    updatequestion();
                }
                else
                {
                    Toast.makeText(Quizeactivity.this,"Wrong",Toast.LENGTH_SHORT).show();

                }
            }
        } );

    }

    public void updatequestion(){
        if(mquestionNumber==6)
            mquestionNumber=0;
        mquestionview.setText(qlib.getquestion(mquestionNumber));
        mchoice1.setText(qlib.getchoice1(mquestionNumber));
        mchoice2.setText(qlib.getchoice2(mquestionNumber));
        mchoice3.setText(qlib.getchoice3(mquestionNumber));
        mchoice4.setText(qlib.getchoice4(mquestionNumber));
        manswer= qlib.getcorrectanswer(mquestionNumber);
        mquestionNumber++;

    }
      private  void  updateScore(int i)
      {
          mscoreview.setText(" "+mscore);
      }
        public void quitting()
        {
            finish();
            System.exit(0);
        }

}
